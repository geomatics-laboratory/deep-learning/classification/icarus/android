package br.ufms.facom.icarus.application.contracts;

import android.net.Uri;

import java.io.File;

import br.ufms.facom.icarus.application.interfaces.MyView;
import br.ufms.facom.icarus.application.interfaces.Presenter;

/**
 * Informações sobre a criação do arquivo.
 * Autor: Mário de Araújo Carvalho
 * E-mail: mariodearaujocarvalho@gmail.com
 * GitHub: https://github.com/MarioDeAraujoCarvalho
 * Ano: 13/5/2019
 * Entrar em contado para maiores informações..
 */
public class ImagePresenter implements Presenter {

    private final MyView view;

    public ImagePresenter(MyView view) {
        this.view = view;
    }

    @Override
    public void cameraClick() {
        if (!view.checkPermission()) {
            view.showPermissionDialog();
            return;
        }
        if (view.availableDisk() <= 5) {
            view.showNoSpaceDialog();
            return;
        }

        File file = view.newFile();

        if (file == null) {
            view.showErrorDialog();
            return;
        }

        view.startCamera(file);
    }

    @Override
    public void ChooseGalleryClick() {
        if (!view.checkPermission()) {
            view.showPermissionDialog();
            return;
        }

        view.chooseGallery();
    }

    @Override
    public void saveImage(Uri uri) {

    }

    @Override
    public void permissionDenied() {
        view.showPermissionDialog();
    }

    @Override
    public void showPreview(String mFilePath) {
        view.displayImagePreview(mFilePath);
    }

    @Override
    public void showPreview(Uri mFileUri) {
        view.displayImagePreview(mFileUri);
    }
}
