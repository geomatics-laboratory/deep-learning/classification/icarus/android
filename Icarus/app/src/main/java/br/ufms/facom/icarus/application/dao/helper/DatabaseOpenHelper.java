package br.ufms.facom.icarus.application.dao.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Informações sobre a criação do arquivo.
 * Autor: Mário de Araújo Carvalho
 * E-mail: mariodearaujocarvalho@gmail.com
 * GitHub: https://github.com/MarioDeAraujoCarvalho
 * Ano: 13/5/2019
 * Entrar em contado para maiores informações..
 */
/**
 * Essa classe serve de interface para criar e abrir o banco de dados.
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper{
	/**
	 * Contexto da Classe
	 */
	private Context mContext;

	/**
	 * Versao do banco de dados
	 */
	public static final int DATA_VERSION_ID = 22;

	/**
	 * Nome do banco de dados
	 */
	public static final String DATABASE_NAME = "database_icarus_v1.db";

	/**
	 * Nome da tabela de itens.
	 */
	public static final String TABLE_ITENS = "tb_itens";
	/**
	 * Campos da tabela de itens.
	 */
	public static final String
			TABLE_ITENS_COLUMN_ID = "id",

			TABLE_ITENS_COLUMN_RESULT_CLASSIFIER_NAME = "result_classifier_name",
			TABLE_ITENS_COLUMN_RESULT_CLASSIFIER_CONFIDENCE = "result_classifier_confidence",

			TABLE_ITENS_COLUMN_RESULT_IS_CORRECT = "result_is_correct",
			TABLE_ITENS_COLUMN_RESULT_CORRECT_CLASS_NAME = "result_correct_class_name",

			TABLE_ITENS_COLUMN_PATH_PICTURE = "path_picture",

			TABLE_ITENS_COLUMN_CREATED = "created",
			TABLE_ITENS_COLUMN_UPDATED = "updated",

			TABLE_ITENS_COLUMN_LATITUDE = "latitude",
			TABLE_ITENS_COLUMN_LONGITUDE = "longitude",
			TABLE_ITENS_COLUMN_ALTITUDE = "altitude",

			TABLE_ITENS_COLUMN_ANGLE_X = "angle_x",
			TABLE_ITENS_COLUMN_ANGLE_Y = "angle_y",
			TABLE_ITENS_COLUMN_ANGLE_Z = "angle_z",

			TABLE_ITENS_COLUMN_IS_FAVORITE = "is_favorite";
	/**
	 * String SQL para a criacao da tabela de itens.
	 */
	public static final String SQL_CREATE_TABLE_ITENS = "CREATE TABLE " + TABLE_ITENS + " ( "

			+ TABLE_ITENS_COLUMN_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, "

			+ TABLE_ITENS_COLUMN_RESULT_CLASSIFIER_NAME + " TEXT NOT NULL, "
			+ TABLE_ITENS_COLUMN_RESULT_CLASSIFIER_CONFIDENCE + " REAL NOT NULL, "

			+ TABLE_ITENS_COLUMN_RESULT_IS_CORRECT + " INTEGER DEFAULT 1, "
			+ TABLE_ITENS_COLUMN_RESULT_CORRECT_CLASS_NAME + " TEXT NOT NULL, "

			+ TABLE_ITENS_COLUMN_PATH_PICTURE + " TEXT NOT NULL, "

			+ TABLE_ITENS_COLUMN_CREATED + " TEXT NOT NULL, "
			+ TABLE_ITENS_COLUMN_UPDATED + " TEXT NOT NULL, "

			+ TABLE_ITENS_COLUMN_LATITUDE + " REAL, "
			+ TABLE_ITENS_COLUMN_LONGITUDE + " REAL, "
			+ TABLE_ITENS_COLUMN_ALTITUDE + " REAL, "

			+ TABLE_ITENS_COLUMN_ANGLE_X + " REAL, "
			+ TABLE_ITENS_COLUMN_ANGLE_Y + " REAL, "
			+ TABLE_ITENS_COLUMN_ANGLE_Z + " REAL, "

			+ TABLE_ITENS_COLUMN_IS_FAVORITE + " INTEGER DEFAULT 0) ";

	/**
	 * Construtor da classe.
	 */
	public DatabaseOpenHelper(Context context){
		super(context, DATABASE_NAME, null, DATA_VERSION_ID);
		mContext = context;
	}

	/**
	 * Esta funcao eh chamada para a criacao da base de dados, no caso de nao existir.
	 */
	@Override
	public void onCreate(SQLiteDatabase database){
		/*
		 * Executa a criacao de cada uma das tabela com base nas strings SQL definidas.
		 */
		database.execSQL(SQL_CREATE_TABLE_ITENS);
		Log.e("Create table --> ", SQL_CREATE_TABLE_ITENS);

	}

	/**
	 * Esta funcao eh chamada no caso de a versao de ID mudar, para alterar a estrutura ou dropar os dados
	 */
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
		Log.d("DEBUG", "DB_OLD_VERSION : " + oldVersion + ", DB_NEW_VERSION : " + newVersion);

		/*
		 * Apaga a tabela atual caso haja atualizacoes.
		 */
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_ITENS);

		/**
		 * Apos apagar a tabela do banco de dados eh necessario cria-la novamente.
		 */
		onCreate(database);
	}
}