package br.ufms.facom.icarus.application.util;

/**
 * Created by MARIO on 21/10/2016.
 *Descricao: Pasta utilitaria para colar partes de codigos temporarias e importantes...
 */

public class Glue {

    int a = 5;
    Integer b = 9;
    /**
     * Loren Ipsum dolor sit amet...
     *
    //Limpa a base de dados para uma total atualização nos dados
    if(quantidade > 0){
        limparDados();
    }

    public boolean limparDados(){
        //Limpar base atual
        try{
            mDatabaseExtras.open();
            mDatabaseExtras.clear();
            mDatabaseExtras.close();
            return true;
        }catch (Exception e){
            Log.e("TAG: ","Não limpou a Base de Dados...");
            return false;
        }
    }
     // ItemAcitivity.java
     //    float[] mGravity;
     //    float[] mGeomagnetic;
     //    public void onSensorChanged(SensorEvent event) {
     //         /** https://developer.android.com/reference/android/hardware/SensorManager.html#getOrientation(float[],%20float[])
     //         * values[0]: Azimuth, angle of rotation about the -z axis. This value represents the angle between the device's y axis and the magnetic north pole. When facing north, this angle is 0, when facing south, this angle is π. Likewise, when facing east, this angle is π/2, and when facing west, this angle is -π/2. The range of values is -π to π.
     //         * values[1]: Pitch, angle of rotation about the x axis. This value represents the angle between a plane parallel to the device's screen and a plane parallel to the ground. Assuming that the bottom edge of the device faces the user and that the screen is face-up, tilting the top edge of the device toward the ground creates a positive pitch angle. The range of values is -π to π.
     //         * values[2]: Roll, angle of rotation about the y axis. This value represents the angle between a plane perpendicular to the device's screen and a plane perpendicular to the ground. Assuming that the bottom edge of the device faces the user and that the screen is face-up, tilting the left edge of the device toward the ground creates a positive roll angle. The range of values is -π/2 to π/2.
     //
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                // orientation contains: azimut, pitch and roll
                double azimuth = (180 * orientation[0]) / Math.PI;// azimut - z
                double pitch = (180 * orientation[1]) / Math.PI; // pitch - x
                double roll = (180 * orientation[2]) / Math.PI;  // roll - y

                mItem.setAngleZ((float) azimuth);
                mItem.setAngleX((float) pitch);
                mItem.setAngleY((float) roll);
                mTxtSensorAngles.setText("X: " + mItem.getAngleX() + "\nY: " + mItem.getAngleY() + "\nZ: " + mItem.getAngleZ() + "\n");

     //        mTxtSensorAngles.setText("Angles(z,x): " + Math.atan2(azimuth, roll)/(Math.PI/180) + "\n" +
     //                "Angles(z,y): " + Math.atan2(azimuth, roll)/(Math.PI/180) + "\n" +
     //                "Angles(x,y): " + Math.atan2(roll, pitch)/(Math.PI/180) + "\n" +
     //                "");

            }
        }
    }

     //
     //            test("Minha mensagem...");
     //
     //            runOnUiThread(
     //                    new Runnable() {
     //                        @Override
     //                        public void run() {
     //
     ////                                        showResultsInBottomSheet(results);
     ////                                        showFrameInfo(previewWidth + "x" + previewHeight);
     ////                                        showCropInfo(cropCopyBitmap.getWidth() + "x" + cropCopyBitmap.getHeight());
     ////                                        showCameraResolution(canvas.getWidth() + "x" + canvas.getHeight());
     ////                                        showRotationInfo(String.valueOf(sensorOrientation));
     ////                                        showInference(lastProcessingTimeMs + "ms");
     //                        }
     //                    });

    */
    public Glue() {
        System.out.print("Teste");
    }
}