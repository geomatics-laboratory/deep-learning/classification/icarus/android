/**
 * Informações sobre a criação do arquivo.
 * Autor: Mário de Araújo Carvalho
 * E-mail: mariodearaujocarvalho@gmail.com
 * GitHub: https://github.com/MarioDeAraujoCarvalho
 * Ano: 13/5/2017
 * Entrar em contado para maiores informações.
 */

package br.ufms.facom.icarus.application.view.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import br.ufms.facom.icarus.BuildConfig;
import br.ufms.facom.icarus.application.contracts.ImagePresenter;
import br.ufms.facom.icarus.application.dao.item.ItemDAO;
import br.ufms.facom.icarus.application.interfaces.MyView;
import br.ufms.facom.icarus.application.model.Item;
import br.ufms.facom.icarus.R;
import br.ufms.facom.icarus.application.util.LocationTrack;
import br.ufms.facom.icarus.application.util.Util;
import br.ufms.facom.icarus.learning.classification.tflite.Classifier;
import br.ufms.facom.icarus.learning.classification.tflite.ClassifierFloatMobileNet;
import br.ufms.facom.icarus.learning.communs.environment.ImageUtils;
import br.ufms.facom.icarus.learning.communs.environment.Logger;
import br.ufms.facom.icarus.learning.communs.customview.env.BorderedText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemActivity extends AppCompatActivity implements MyView, SensorEventListener {
    // Sensos x, y, z
    private SensorManager mSensorManager;
    Sensor accelerometer;

    private Runnable postInferenceCallback;
    private static final Logger LOGGER = new Logger();
    private long lastProcessingTimeMs;
    private Classifier classifier;
    // Codes of result
    static final int REQUEST_TAKE_PHOTO = 101;
    static final int REQUEST_GALLERY_PHOTO = 102;
    // Permissions on app
    static String[] permissions = new String[]{
            Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    // TextViews
    @BindView(R.id.txt_item_view_details_geo_location) TextView mTxtGeoLocation;
    @BindView(R.id.txt_item_view_details_sensor_angles) TextView mTxtSensorAngles;
    // EditTexts
    @BindView(R.id.txt_item_confidence_percent) EditText txtConfidence;
    @BindView(R.id.txt_item_class_item) EditText mTxtClassItem;
    // Layouts
    @BindView(R.id.ll_root_info) LinearLayout mLinearLayoutInfo;
    // Toolbar
    @BindView(R.id.toolbar) Toolbar mToolbar;
    // ImageButton
    @BindView(R.id.user_profile_photo) ImageButton userProfilePhoto;

    private ImagePresenter mPresenter;
    Uri photoURI;
    String finalPathPhoto = "";

    private ItemDAO itemDAO;
    private Item mItem;

    private int codigo = -1;

    LocationTrack locationTrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        ButterKnife.bind(this);
        mPresenter = new ImagePresenter(this);
        mToolbar.setNavigationIcon(R.mipmap.ic_menu_seta_esquerda_white);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            w.setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary, getTheme()));
        }else{
            //noinspection deprecation
            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }

        // Register the sensor listeners
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        codigo = getIntent().getExtras().getInt("codigo", -1);
        itemDAO = new ItemDAO(this);
        // Inits
        mTxtSensorAngles.setVisibility(View.GONE);

        fillFilds();
        initClassifier();

    }
    public void initClassifier(){
        // Init classifier
        try {
            classifier = new ClassifierFloatMobileNet(this);
            LOGGER.d("Success when loading classifier...");
        } catch (IOException e) {
            LOGGER.e(e, "Failed to load classifier!");
            return;
        }
    }

    public void fillFilds(){
        if(codigo != -1){
            itemDAO.open();
            mItem = itemDAO.getItem(codigo);
            itemDAO.close();

            mTxtClassItem.setText(mItem.getResultCorrectClassName());
            txtConfidence.setText(String.valueOf(mItem.getResultClassifierConfidence()));

            // set picture
            finalPathPhoto = mItem.getPathPicture();
            displayImagePreview(finalPathPhoto);

            mTxtGeoLocation.setText("Latitude: " + (mItem.getLatitude()) + "\nLongitude: " + (mItem.getLongitude() + "\nAltitude: " + (mItem.getAltitude())));
            mTxtSensorAngles.setText("X: " + mItem.getAngleX() + "\nY: " + mItem.getAngleY() + "\nZ: " + mItem.getAngleZ() + "\n");

        }else{
            mItem = new Item();
        }
    }

    public boolean save(){
        mItem.setId(codigo);

        if(!mTxtClassItem.getText().toString().equals("")){
            mItem.setResultCorrectClassName(mTxtClassItem.getText().toString());
        }else{
            Util.showAlertaBuilder(this, getResources().getString(R.string.text_aviso_campo_obrigatorio)+": nome do mItem!");
            mTxtClassItem.requestFocus();
            return false;
        }

        if(!txtConfidence.getText().toString().equals("")){
            mItem.setResultClassifierConfidence(Float.parseFloat(txtConfidence.getText().toString()));
        }else{
            Util.showAlertaBuilder(this,getResources().getString(R.string.text_aviso_campo_obrigatorio)+": descrição!");
            txtConfidence.requestFocus();
            return false;
        }

        if(!finalPathPhoto.isEmpty()){
            mItem.setPathPicture(finalPathPhoto);
        }else{
            Util.showAlertaBuilder(this,getResources().getString(R.string.text_aviso_campo_obrigatorio)+": foto!");
            return false;
        }

        itemDAO.open();
        if(codigo == -1){
            itemDAO.insert(mItem);
        }else{
            itemDAO.update(mItem, mItem.getId());
        }
        itemDAO.close();

        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {  }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub
        if (event.sensor != accelerometer)
            return;

        float roll= event.values[0]; // x
        float pitch= event.values[1]; // y
        float azimuth= event.values[2]; // z

        double angleX = Math.atan2(roll, pitch)/(Math.PI/180);
        double angleZ = Math.atan2(azimuth, pitch)/(Math.PI/180);

        mTxtSensorAngles.setText("Rotation in x-axis: " + angleX + "\n" +
                "Rotation in z-axis:" + angleZ);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(locationTrack != null){
            locationTrack.stopListener();
        }
    }

    protected void processImage() {
        LOGGER.d("Processing image...");
        if (classifier == null) {
            return;
        }
        final long startTime = SystemClock.uptimeMillis();
        File file = new File(finalPathPhoto);
        if (file.exists()) {
            Bitmap photoBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            Bitmap croppedBitmap = ImageUtils.getCroppedBitmap(photoBitmap);

            final List<Classifier.Recognition> results = classifier.recognizeImage(croppedBitmap);

            lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;
            LOGGER.i("Detect: %s", results);
            // cropCopyBitmap = Bitmap.createBitmap(croppedBitmap);
            Classifier.Recognition recognition = results.get(0);
            if (recognition != null) {
                if (recognition.getTitle() != null) mTxtClassItem.setText(recognition.getTitle());
                if (recognition.getConfidence() != null){
                    LOGGER.d("Confidence: " + recognition.getConfidence());
                    txtConfidence.setText(String.valueOf(100 * recognition.getConfidence()));
                }

            }
        } else {
            LOGGER.d("File note exists...");
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_adicionar_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
            overridePendingTransition(R.anim.slide_right_enter, R.anim.slide_right_exit);
        }else if(item.getItemId() == R.id.action_adicionar){
            if(save()){
                finish();
                overridePendingTransition(R.anim.slide_right_enter, R.anim.slide_right_exit);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_enter, R.anim.slide_right_exit);
    }

    @Override
    public void startCamera(File file) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            if (file != null) {
                photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider", file);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }

    public void setLocationTrack() {
        // Location
        try {
            locationTrack = new LocationTrack(ItemActivity.this);
            if (locationTrack.canGetLocation()) {

                float longitude = locationTrack.getLongitude();
                float latitude = locationTrack.getLatitude();
                float altitude = locationTrack.getAltitude();

                Toast.makeText(getApplicationContext(), "Longitude:" + Double.toString(longitude)
                                + "\nLatitude:" + Double.toString(latitude)
                                + "\nAltitude:" + Double.toString(altitude),
                        Toast.LENGTH_SHORT).show();

                mItem.setLatitude(latitude);
                mItem.setLongitude(longitude);
                mItem.setAltitude(altitude);

                mTxtGeoLocation.setText("Latitude: " + (mItem.getLatitude()) + "\nLongitude: " + (mItem.getLongitude() + "\nAltitude: " + (mItem.getAltitude())));

            } else {
                locationTrack.showSettingsAlert();
            }
        }catch (Exception e){
            Util.showToast(ItemActivity.this, e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    @Override
    public void chooseGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                LOGGER.d("ResultTake:" + getFilePath().getAbsolutePath() + photoURI.getLastPathSegment());
                finalPathPhoto = getFilePath().getAbsolutePath() + "/" +photoURI.getLastPathSegment();
                mPresenter.showPreview(photoURI);
                processImage();
                setLocationTrack();

            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                String mPhotoPath = getRealPathFromUri(selectedImage);
                finalPathPhoto = mPhotoPath;
                mPresenter.showPreview(mPhotoPath);
                processImage();

            }
        }
    }

    @Override
    public boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, mPermission);
            if (result == PackageManager.PERMISSION_DENIED) return false;
        }
        return true;
    }

    @Override
    public void showPermissionDialog() {
        Dexter.withActivity(this).withPermissions(permissions)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            selectImage();
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                showErrorDialog();
            }
        })
                .onSameThread()
                .check();
    }

    @Override
    public File getFilePath() {
        return getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public void showSettingsDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.message_need_permission));
        builder.setMessage(getString(R.string.message_grant_permission));
        builder.setPositiveButton(getString(R.string.label_setting), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void showNoSpaceDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.error_message_no_more_space));
        builder.setMessage(getString(R.string.error_message_insufficient_space));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public int availableDisk() {
        File mFilePath = getFilePath();
        long freeSpace = mFilePath.getFreeSpace();
        return Math.round(freeSpace / 1048576);

    }

    @Override
    public File newFile() {
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTimeInMillis();
        String mFileName = timeInMillis + ".jpeg";
        File mFilePath = getFilePath();
        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            newFile.createNewFile();
            LOGGER.d("NewFile: " + newFile.getAbsolutePath());
            return newFile;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void showErrorDialog() {
        Toast.makeText(getApplicationContext(), getString(R.string.error_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayImagePreview(String mFilePath) {
        LOGGER.d("Display image preview (path): " + mFilePath);
        Glide.with(ItemActivity.this).load(mFilePath).apply(new RequestOptions().centerCrop().circleCrop().placeholder(R.mipmap.ic_camera_white)).into(userProfilePhoto);
    }

    @Override
    public void displayImagePreview(Uri mFileUri) {
        LOGGER.d("Display image preview (URI): " +  mFileUri.getLastPathSegment());
        Glide.with(ItemActivity.this).load(mFileUri).apply(new RequestOptions().centerCrop().circleCrop().placeholder(R.mipmap.ic_camera_white)).into(userProfilePhoto);
    }

    /**
     * Get real file path from URI
     * @param contentUri
     * @return
     */
    @Override
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_gallery),
                getString(R.string.cancel)};
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(ItemActivity.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    mPresenter.cameraClick();
                } else if (items[item].equals("Choose from Gallery")) {
                    mPresenter.ChooseGalleryClick();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @OnClick({R.id.user_profile_photo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.user_profile_photo:
                selectImage();
                break;
        }
    }

}
