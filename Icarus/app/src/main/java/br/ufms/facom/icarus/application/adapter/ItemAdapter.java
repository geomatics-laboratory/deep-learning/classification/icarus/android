package br.ufms.facom.icarus.application.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import br.ufms.facom.icarus.R;
import br.ufms.facom.icarus.application.dao.item.ItemDAO;
import br.ufms.facom.icarus.application.interfaces.OnClickListener;
import br.ufms.facom.icarus.application.interfaces.OnItemLongClickListener;
import br.ufms.facom.icarus.application.model.Item;
import br.ufms.facom.icarus.application.util.RoundedImageView;
import br.ufms.facom.icarus.application.util.Util;
/**
 * Informações sobre a criação do arquivo.
 * Autor: Mário de Araújo Carvalho
 * E-mail: mariodearaujocarvalho@gmail.com
 * GitHub: https://github.com/MarioDeAraujoCarvalho
 * Ano: 13/5/2019
 * Entrar em contado para maiores informações..
 */
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    private int mBackground;
    private Context mContext;
    private HashSet<Integer> checkedItems;
    private List<Item> mListItems;
    private ArrayList<Item> mSearchItems;

    private static OnClickListener mOnClickListener;
    private static OnItemLongClickListener mOnItemLongClickListener;


    private static final int ITEM_VIEW_TYPE_ITEM = 1;
    private static final int ITEM_VIEW_TYPE_HEADER = 0;

    public ItemAdapter(Context context, List<Item> items) {
        this.mContext = context;
        this.mListItems = items;
        this.checkedItems = new HashSet<>();
        this.mSearchItems = new ArrayList<>();
        this.mSearchItems.addAll(this.mListItems);

        if (context != null) {
            TypedValue mTypedValue = new TypedValue();
            context.getTheme().resolveAttribute(android.R.attr.selectableItemBackground, mTypedValue, true);
            this.mBackground = mTypedValue.resourceId;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View mContainer;

        public TextView mItemClass, mItemConfidence;
        public ImageView mImageIsNotFavorite, mImageIsFavorite;
        public ImageView mImageItem;

        public ViewHolder(View view) {
            super(view);

            mContainer = view;

            mItemClass = (TextView) view.findViewById(R.id.txt_item_class_item);
            mItemConfidence = (TextView) view.findViewById(R.id.txt_item_confidence_percent);
            mImageItem = (RoundedImageView) view.findViewById(R.id.img_card_view_item_card);

            mImageIsNotFavorite = (ImageView) view.findViewById(R.id.img_is_not_favorite_item_card);
            mImageIsFavorite = (ImageView) view.findViewById(R.id.img_is_favorite_item_card);

            mContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // -1 Refers to the header
                    Log.e("TAG", "onClick");

                    int position = getAdapterPosition();
                    if (mOnClickListener != null && position >= 0) {
                        mOnClickListener.onClick(v, position);
                    }
                }
            });

            mContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.e("TAG", "onLongClick");

                    int position = getAdapterPosition();
                    if (mOnItemLongClickListener != null && position >= 0) {
                        mOnItemLongClickListener.onLongClick(v, position);
                    }
                    return true;
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView;

        mItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_card_view, parent, false);
        mItemView.setBackgroundResource(this.mBackground);
        return new ViewHolder(mItemView);
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? ITEM_VIEW_TYPE_HEADER : ITEM_VIEW_TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(final ViewHolder mHolder, int position) {

        if (mListItems == null || mListItems.size() == 0){
            return;
        }else{
            final Item mItem = mListItems.get(position);

            mHolder.mItemClass.setText(mItem.getResultCorrectClassName());
            mHolder.mItemConfidence.setText(String.valueOf(mItem.getResultClassifierConfidence()));
            Glide.with(mContext).load(mItem.getPathPicture()).apply(new RequestOptions().centerCrop().placeholder(R.mipmap.ic_camera_white)).into(mHolder.mImageItem);


            if(mItem.getIsFavorite() == 1){
                mHolder.mImageIsFavorite.setVisibility(View.VISIBLE);
                mHolder.mImageIsNotFavorite.setVisibility(View.GONE);
            }else{
                mHolder.mImageIsFavorite.setVisibility(View.GONE);
                mHolder.mImageIsNotFavorite.setVisibility(View.VISIBLE);
            }
            mHolder.mImageIsNotFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    favoritar(mItem);
                }
            });

            mHolder.mImageIsFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    favoritar(mItem);
                }
            });

        }
    }

    public void favoritar(Item mItem) {

        String texto = "";
        if(mItem.getIsFavorite() == 1){
            mItem.setIsFavorite(0);
            texto = "Retirado dos Favoritos!";
        }else{
            mItem.setIsFavorite(1);
            texto = "Adicionado dos Favoritos!";
        }
        ItemDAO mItemDAO = new ItemDAO(mContext);
        mItemDAO.open();
        mItemDAO.updateIsFavorite(mItem);
        mItemDAO.close();

        //Atualiza o item
        this.notifyDataSetChanged();
    }


    public void remove(int position){
        mListItems.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        mOnItemLongClickListener = onItemLongClickListener;
    }

    public void resetarCheck() {
        this.checkedItems.clear();
        this.notifyDataSetChanged();
    }

    public void setChecked(int position, boolean checked) {
        resetarCheck();

        if (checked) {
            this.checkedItems.add(position);
        } else {
            this.checkedItems.remove(position);
        }

        this.notifyDataSetChanged();
    }

    public void searchWords(CharSequence charText) {

        charText = Util.removeAccent((String) charText).toLowerCase(Locale.getDefault());

        mListItems.clear();
        if (charText.length() == 0) {
            mListItems.addAll(mSearchItems);
        } else {
            for (Item mItem : mSearchItems) {
                String classItem = Util.removeAccent(mItem.getResultCorrectClassName());
                String confidenceItem = Util.removeAccent(String.valueOf(mItem.getResultClassifierConfidence()));

                if (classItem.toLowerCase(Locale.getDefault()).contains(charText) || confidenceItem.toLowerCase(Locale.getDefault()).contains(charText)) {
                    mListItems.add(mItem);
                }
            }
        }

        notifyDataSetChanged();
    }
}

