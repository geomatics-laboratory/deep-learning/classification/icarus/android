package br.ufms.facom.icarus.application.util;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.widget.EditText;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.channels.NoConnectionPendingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;

/**
 * Esta é a classe mais util de todas as classes do pacote util rsrsrsr.
 */
public class Util {
    private final static boolean DEBUG = true;
    private final static String TAG = "MY_TAG";
    private final static String TEXTO_CONTINUAR_COM = "Continuar com..";
    /**
     * Converte um objeto do tipo Bitmap para byte[] (array de bytes)
     *
     * @param imagem
     * @param qualidadeDaImagem
     * @return
     */
    public static byte[] convertBitmapInByteArray(Bitmap imagem,
                                                    int qualidadeDaImagem) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        imagem.compress(CompressFormat.JPEG, qualidadeDaImagem, stream);
        return stream.toByteArray();
    }

    /**
     * Converte um objeto do tipo byte[] (array de bytes) para Bitmap
     *
     * @param imagem
     * @return
     */
    public static Bitmap convertByteArrayInBitmap(byte[] imagem) {
        return BitmapFactory.decodeByteArray(imagem, 0, imagem.length);
    }

    /**
     * Mostra uma Mensagem dentro de uma caixa de dialogo e fica
     * congelada na tela até que se prescione OK, ou o botão onBackPressed().
     * É melhor que o Toast porque contem um tento maior para ser lido
     *
     * @param sms : String contendo o texto que se deseja mostrar
     * @param context : Contexto da Aplicação
     * mostra o AlertDialog.Builder
     */

    public static void showAlertaBuilder(Context context, String sms) {

        AlertDialog.Builder alerta = new AlertDialog.Builder(context);
        alerta.setMessage(""+sms);
        alerta.setNeutralButton("OK",null);
        alerta.create().show();
    }

    public static void showAlertaBuilder(Context context, String title, String sms) {

        AlertDialog.Builder alerta = new AlertDialog.Builder(context);
        alerta.setTitle(title);
        alerta.setMessage(""+sms);
        alerta.setNeutralButton("OK",null);
        alerta.create().show();
    }
    /**
     * Mostra uma Mensagem em um Toast e o congelamento depende da duração passada: LENGTH_LONG ou LENGTH_SHORT
     * É um metódo util para poder facilitar e acelerar a criação de Toasts.
     *
     * @param sms : String contendo o texto que se deseja mostrar
     * @param context : Contexto da Aplicação
     * @param  duracao : Tempo de Duração do Toast: LENGTH_LONG ou LENGTH_SHORT
     * mostra o Toast
     */

    public static void showToast(Context context,String sms,int duracao) {

        Toast toast = Toast.makeText(context, sms, duracao);
        toast.show();
    }

    /**
     *
     * @param context: Context para Intent
     * @param numero: Que se realizara a ligacao
     */

    public static void callPhone(Context context, String numero)
    {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+numero));

        try {
            Intent chooser = Intent.createChooser(intent, TEXTO_CONTINUAR_COM);
            context.startActivity(chooser);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "Error: Não foi possível realizar chamada!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     *
     * @param context
     * @param numero
     * @param texto
     */
    public static void sendSMS(Context context, String numero, String texto)
    {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("sms:"+numero));
        intent.putExtra("sms_body", texto);

        try {
            Intent chooser = Intent.createChooser(intent, TEXTO_CONTINUAR_COM);
            context.startActivity(chooser);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "Error: Não foi possível enviar SMS!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     *
     * @param context
     * @param texto
     */
    public static void sharedTextSocial(Context context, String texto){

        try{
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            final StringBuilder builder = new StringBuilder();
            builder.append(texto);
            builder.append("\n");
            i.putExtra(Intent.EXTRA_TEXT, builder.toString());
            context.startActivity(Intent.createChooser(i, TEXTO_CONTINUAR_COM));
        }catch (Exception e){
            AlertDialog.Builder enviarEmail = new AlertDialog.Builder(context);
            enviarEmail.setMessage("Seu dispositivo não suporta essa função!");
            enviarEmail.create().show();
        }
    }

    /**
     *
     * @param context: Context da Intent
     * @param url: URL para abrir
     */
    public static void openURL(Context context, String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        try {
            Intent chooser = Intent.createChooser(intent, TEXTO_CONTINUAR_COM);
            context.startActivity(chooser);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "Error: Não foi possível abrir a URL", Toast.LENGTH_SHORT).show();
        }
    }

    public static void sendEmail(Context context, String endereco_receptor, String subject, CharSequence texto_conteudo)
    {
        Intent mailIntent = new Intent(Intent.ACTION_SEND);
        mailIntent.setType("message/rfc822");
        mailIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{endereco_receptor});
        mailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        mailIntent.putExtra(Intent.EXTRA_TEXT   , texto_conteudo);

        try {
            Intent chooser = Intent.createChooser(mailIntent, TEXTO_CONTINUAR_COM);
            context.startActivity(chooser);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "Error: Não foi possível enviar o E-mail!", Toast.LENGTH_SHORT).show();
        }
    }

    public static void startIntent(Context context, AppCompatActivity activity)
    {
        Intent newIntent = new Intent(context.getApplicationContext(), activity.getClass());
        context.startActivity(newIntent);

    }
    public static void startIntentParseOneValue(Context context, AppCompatActivity activity, String key, int value)
    {
        Intent newIntent = new Intent(context, activity.getClass());
        newIntent.putExtra(key,value);
        context.startActivity(newIntent);
    }

    public static String removeAccent(String text){
        String result = Normalizer.normalize(text, Normalizer.Form.NFD);
        return result.replaceAll("[^\\p{ASCII}]", "");
    }

    public static String replacePhone(String phone) {
        String result = phone.replace("(", "");
        result = result.replace(")", "");
        result = result.replace(".", "");
        result = result.replace("-", "");
        return result;
    }


    /**
     * Máscara textos
     * Exemplo de Utilizacao
     txtCEP = (EditText)findViewById(R.id.txt_cep);
     TextWatcher cepMask = Util.Mask.insert("######-###", txtCEP);
     txtCEP.addTextChangedListener(cepMask);
     *
     * */

    public abstract static class Mask {

        /**
         * @Description: Tira a mascara
         * @param text
         * @return
         */
        public static String unmask(String text) {
            return text.replaceAll("[.]", "").replaceAll("[-]", "")
                    .replaceAll("[/]", "").replaceAll("[(]", "")
                    .replaceAll("[)]", "");
        }

        /**
         * Inseri a máscára em tempo real no EditText
         * @param mask
         * @param ediTxt
         * @return
         */
        public static TextWatcher insert(final String mask, final EditText ediTxt) {

            return new TextWatcher() {
                boolean isUpdating;
                String old = "";

                public void onTextChanged(CharSequence textoParaMascarar, int start,
                                          int before, int count) {
                    String texto = Mask.unmask(textoParaMascarar.toString());
                    String mascara = "";

                    if (isUpdating) {
                        old = texto;
                        isUpdating = false;
                        return;
                    }

                    int i = 0;
                    for (char m : mask.toCharArray()) {
                        if (m != '#' && texto.length() > old.length()) {
                            mascara += m;
                            continue;
                        }
                        try {
                            mascara += texto.charAt(i);
                        } catch (Exception e) {
                            break;
                        }
                        i++;
                    }

                    isUpdating = true;
                    ediTxt.setText(mascara);
                    ediTxt.setSelection(mascara.length());
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void afterTextChanged(Editable s) {
                }
            };
        }
    }

    public static CharSequence getSuperscriptString(CharSequence base) {
        return getSuperscriptString(base, 0, base.length() - 1);
    }

    public static CharSequence getSuperscriptString(CharSequence base, int startFromIdx, int endAtIdx) {
        SpannableString str = new SpannableString(base);
        str.setSpan(new SuperscriptSpan(), startFromIdx, endAtIdx, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return str;
    }

    public static CharSequence getSubscriptString(CharSequence base) {
        return getSubscriptString(base, 0, base.length() - 1);
    }

    public static CharSequence getSubscriptString(CharSequence base, int startFromIdx, int endAtIdx) {
        SpannableString str = new SpannableString(base);
        str.setSpan(new SubscriptSpan(), startFromIdx, endAtIdx, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return str;
    }

    public static SpannableStringBuilder getSub(String texto){
        SpannableStringBuilder textSub = new SpannableStringBuilder("<pre>"+texto+"<br/></pre>");
        textSub.setSpan(new SubscriptSpan(), 0, textSub.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        /**Deve ser setado no TextView da seguinte forma:
         * mTextView.setText(textSub_Sup, TextView.BufferType.SPANNABLE);
         * */

        return textSub;
    }


    @SuppressWarnings("deprecation")
    public static Spanned getTextHTML(String texto){

        Spanned string;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            string = Html.fromHtml(texto, Html.FROM_HTML_MODE_LEGACY);
        } else{
            string = Html.fromHtml(texto);
        }

        return string;
    }
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    /**
     * O metodo getNetworkInfo da classe ConnectivityManager esta Deprecated.
     */
    public static boolean verifyConnection(Context context) {
        /* ConnectivityManager
        *.TYPE_MOBILE 0
        *.TYPE_WIFI 1
        *.TYPE_WIMAX 6
        *.TYPE_ETHERNET 9
        */
        try {
            ConnectivityManager cm = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            int[] p = {0,1,6,9};
            for (int i : p) {
                if (cm.getNetworkInfo(i).isConnected()){
                    //Toast.makeText(context, "Sucesso ao Verificar\n STATUS da rede", Toast.LENGTH_LONG).show();
                    return true;//Conexão OK
                }
            }
        } catch (Exception e) {
            //Toast.makeText(context,"ERRO ao Verificar\n STATUS da rede",Toast.LENGTH_LONG).show();
            //e.printStackTrace();
        }
        return false;//Sem Conexão
    }
    public static boolean checkConnection(Context context) throws NoConnectionPendingException {

        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity == null) {
            return false;
        } else {

            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo anInfo : info) {
                    if (anInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public String hashMD5(String input) {
        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
