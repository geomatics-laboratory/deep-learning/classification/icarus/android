package br.ufms.facom.icarus.application.view.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import br.ufms.facom.icarus.R;
import br.ufms.facom.icarus.application.dao.item.ItemDAO;
import br.ufms.facom.icarus.application.model.Item;
import br.ufms.facom.icarus.application.util.Util;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemDetailsActivity extends AppCompatActivity {

    private ItemDAO mItemDAO;
    private Item mItem;

    // TextViews
    @BindView(R.id.txt_item_view_details_geo_location) TextView mTxtGeoLocation;
    @BindView(R.id.txt_item_view_details_sensor_angles) TextView mTxtSensorAngles;
    // EditTexts
    @BindView(R.id.txt_item_confidence_percent) TextView mTxtConfidenceItem;
    @BindView(R.id.txt_item_class_item) TextView mTxtClassItem;
    // Layouts
    @BindView(R.id.ll_root_info) LinearLayout mLinearLayoutInfo;
    // Toolbar
    @BindView(R.id.toolbar) Toolbar mToolbar;
    // ImageButton
    @BindView(R.id.user_profile_photo)
    ImageView userProfilePhoto;

    @BindView(R.id.fab)
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_item);
        ButterKnife.bind(this);

        mToolbar.setTitle(getResources().getString(R.string.title_activity_details_item));
        mToolbar.setNavigationIcon(R.mipmap.ic_menu_seta_esquerda_white);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        setSupportActionBar(mToolbar);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compartilhar(mItem);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Instancia os objetos utilizados
        mItemDAO = new ItemDAO(ItemDetailsActivity.this);
        mItem = new Item();
        //Recebe o código pelo Dundle
        Bundle mDados = getIntent().getExtras();
        String id = mDados.getString("codigo");
        //Carrega os dados
        loadData(id);
    }

    public void loadData(String id) {
        mItemDAO.open();
        mItem = mItemDAO.getItem(Integer.valueOf(id));
        mItemDAO.close();

        mTxtClassItem.setText(mItem.getResultCorrectClassName());
        mTxtConfidenceItem.setText(String.valueOf(mItem.getResultClassifierConfidence()));
        // set picture
        Glide.with(ItemDetailsActivity.this).load(mItem.getPathPicture()).apply(new RequestOptions().centerCrop().placeholder(R.mipmap.ic_camera_white)).into(userProfilePhoto);

        mTxtGeoLocation.setText("Latitude: " + (mItem.getLatitude()) + "\nLongitude: " + (mItem.getLongitude() + "\nAltitude: " + (mItem.getAltitude())));
        mTxtSensorAngles.setText("X: " + mItem.getAngleX() + "\nY: " + mItem.getAngleY() + "\nZ: " + mItem.getAngleZ() + "\n");
    }


    public void compartilhar(Item item){
        String texto = "Olá Pessoal,\n" +
                "Gostaria de Comparlihar esse item: \n" +
                "\nClasse: "+ item.getResultCorrectClassName()+
                "\nPorcentagem de acerto: " + item.getResultClassifierConfidence()+
                "\n\n\n--\nEnviado de Fast Framework";

        Util.sharedTextSocial(ItemDetailsActivity.this, texto);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_itens_detalhes, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.item_menu_deletar:
                deleteDialog();
                break;
            case R.id.item_menu_editar:
                Util.startIntentParseOneValue(ItemDetailsActivity.this, new ItemActivity(),"codigo",(mItem.getId()));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
        }
        return true;
    }

    private void deleteDialog() {
        AlertDialog.Builder janela = new AlertDialog.Builder(ItemDetailsActivity.this);
        janela.setTitle(getResources().getString(R.string.text_aviso_title));
        janela.setMessage(getResources().getString(R.string.text_aviso_excluir_item));
        janela.setNeutralButton(getResources().getString(R.string.text_aviso_opcao_nao),null);
        janela.setPositiveButton(getResources().getString(R.string.text_aviso_opcao_sim),new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ItemDAO mItemDAO = new ItemDAO(ItemDetailsActivity.this);
                mItemDAO.open();
                mItemDAO.delete(mItem.getId());
                mItemDAO.close();

                dialog.dismiss();
                finish();
            }
        });
        janela.create().show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
