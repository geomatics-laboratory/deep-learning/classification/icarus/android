package br.ufms.facom.icarus.application.interfaces;

import android.net.Uri;

/**
 * Created on : Jan 06, 2019
 * Author     : AndroidWave
 * Website    : https://androidwave.com/
 */
public interface Presenter {

    void cameraClick();

    void ChooseGalleryClick();

    void saveImage(Uri uri);

    void permissionDenied();

    void showPreview(String mFilePath);

    void showPreview(Uri mFileUri);
}