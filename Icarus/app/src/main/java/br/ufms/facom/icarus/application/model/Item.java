package br.ufms.facom.icarus.application.model;

import java.io.Serializable;

/**
 * Created by Mário de Araújo Carvalho on 30/10/17.
 */

public class Item implements Serializable{
    //Atributos da class
    private int id = 0;
    private String resultClassifierName = "";
    private float resultClassifierConfidence = 0;

    private int resultIsCorrect = 1;
    private String resultCorrectClassName = ""; // Os de interrese ou outra

    private String pathPicture = "";

    private String created = "";
    private String updated = "";

    private float latitude = 0;
    private float longitude = 0;
    private float altitude = 0;

    private float angleX = 0;
    private float angleY = 0;
    private float angleZ = 0;

    private int isFavorite = 0;


    //Construtor Vazio
    public Item() {
    }

    public int getResultIsCorrect() {
        return resultIsCorrect;
    }

    public void setResultIsCorrect(int resultIsCorrect) {
        this.resultIsCorrect = resultIsCorrect;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResultClassifierName() {
        return resultClassifierName;
    }

    public void setResultClassifierName(String resultClassifierName) {
        this.resultClassifierName = resultClassifierName;
    }

    public float getResultClassifierConfidence() {
        return resultClassifierConfidence;
    }

    public void setResultClassifierConfidence(float resultClassifierConfidence) {
        this.resultClassifierConfidence = resultClassifierConfidence;
    }

    public String getResultCorrectClassName() {
        return resultCorrectClassName;
    }

    public void setResultCorrectClassName(String resultCorrectClassName) {
        this.resultCorrectClassName = resultCorrectClassName;
    }

    public String getPathPicture() {
        return pathPicture;
    }

    public void setPathPicture(String pathPicture) {
        this.pathPicture = pathPicture;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }

    public float getAngleX() {
        return angleX;
    }

    public void setAngleX(float angleX) {
        this.angleX = angleX;
    }

    public float getAngleY() {
        return angleY;
    }

    public void setAngleY(float angleY) {
        this.angleY = angleY;
    }

    public float getAngleZ() {
        return angleZ;
    }

    public void setAngleZ(float angleZ) {
        this.angleZ = angleZ;
    }

    public int getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        this.isFavorite = isFavorite;
    }
}