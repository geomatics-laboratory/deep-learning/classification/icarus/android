package br.ufms.facom.icarus.application.dao.extras;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.ufms.facom.icarus.application.dao.helper.DatabaseOpenHelper;

/**
 * Informações sobre a criação do arquivo.
 * Autor: Mário de Araújo Carvalho
 * E-mail: mariodearaujocarvalho@gmail.com
 * GitHub: https://github.com/MarioDeAraujoCarvalho
 * Ano: 13/5/2019
 * Entrar em contado para maiores informações..
 */
public class DatabaseExtras {

    protected SQLiteDatabase mSQLiteDatabase;
    protected DatabaseOpenHelper mDatabaseOpenHelper;
    private Context mContext;

    public DatabaseExtras(Context context) {
        this.mContext = context;
        mDatabaseOpenHelper = new DatabaseOpenHelper(mContext);
    }

    public void open() throws SQLException {
        mSQLiteDatabase = mDatabaseOpenHelper.getWritableDatabase();
    }

    public void close() {
        mDatabaseOpenHelper.close();
    }
    /*
     *  Limpa todos os dados do banco
     */
    public void clear(){
        mSQLiteDatabase.delete(DatabaseOpenHelper.TABLE_ITENS, null, null);
        Log.e("LOG: ","Limpeza de base dados realizada com sucesso...");
    }
    /**
     * Testa a base de dados!
     */
    public void test(){
        mSQLiteDatabase.rawQuery("SELECT * FROM "+DatabaseOpenHelper.TABLE_ITENS +" ; ", null);
        Log.e("LOG: ","Sucesso ao testar base de dados...");
    }

}
