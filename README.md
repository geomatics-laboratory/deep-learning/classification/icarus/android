# Icarus - Generic mobile application for remote sensing

Android mobile part of the project, responsible for using a trained and converted neural network to classify classes, which runs on mobile devices with Android operating system.

## Images
![Print 1](photo_icarus_01.jpg "MarineGEO logo")
![Print 2](photo_icarus_02.jpg "MarineGEO logo")



During this work, a generic tool for classifying remote sensing images was developed. This tool proved to be very useful and promising in the tests carried out, thus enabling more agility and practicality in the tests of models on mobile devices.

The developed application has a modern design
extra features for image pre-processing, a database to store the ratings and captured images, real-time rating using the device's camera, rating new photos or uploading an image from the gallery, storage of the image georeferencing with the collection of latitude, longitude and altitude. We developed the application to accept all classification models tested using Tensorflow using good source code programming practices to facilitate maintenance, extension and calculation of inference processing time.

## List of the features
    - **Classification**: The tool is able to classify images using a trained neural network.
    - **Modern design**: The tool is designed to be modern and user-friendly.
    - **Extra features**: The tool has extra features for image pre-processing
    - **Database**: The tool has a database to store the ratings and captured images
    - **Real-time rating**: The tool has a real-time rating using the device's camera
    - **Rating new photos**: The tool has a rating new photos or uploading an image from the gallery
    - **Storage of the image georeferencing**: The tool has storage of the image georeferencing with the collection of latitude, longitude and altitude
    - **Source code**: The tool is written in Java and uses good source code programming practices to facilitate maintenance, extension and calculation of inference processing time.


## List of the requirements
    - *Android*: The tool is compatible with Android devices.
    - *Tensorflow*: The tool is compatible with Tensorflow.
    - *Python*: The tool is compatible with Python.
    - *Android Studio*: The tool is compatible with Android Studio.

 The application was named iCarus by the development team, and its complete source code and other artifacts are freely available under MIT license in the Geomatica laboratory source code repository at the link address: https://gitlab.com/geomatics-laboratory/deep-learning/classification/icarus .

